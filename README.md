# Merchello.Plugins.SendGridNotifications

A plugin for Merchello which allows Notifications to be sent using the SendGrid API v3. 

## Usage

All that is required is installing the package via Nuget. Merchello will automatically detect it and add it to the possible notification providers in the back office. An API key is required, and a template id can be optionally provided to allow email content to be used inside a template managed in SendGrid.

## Running Locally

Can be run using Local IIS or IISExpress. Developed using Visual Studio 2015.

Backoffice Username/Password: admin/password