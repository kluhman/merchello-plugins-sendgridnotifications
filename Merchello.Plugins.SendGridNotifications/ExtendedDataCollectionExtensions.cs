﻿using System;
using System.Collections.Generic;
using System.Linq;

using Merchello.Core.Models;

using Newtonsoft.Json;

using Umbraco.Core;

namespace Merchello.Plugins.SendGridNotifications
{
    public static class ExtendedDataCollectionExtensions
    {
        private const string SendGridNotificationProviderSettingsKey = "sendGridNotificationProviderSettings";

        public static SendGridNotificationProviderSettings GetSendGridNotificationSettings(this ExtendedDataCollection extendedData)
        {
            Mandate.ParameterNotNull(extendedData, nameof(extendedData));

            if (!extendedData.ContainsKey(SendGridNotificationProviderSettingsKey))
            {
                return new SendGridNotificationProviderSettings();
            }

            return JsonConvert.DeserializeObject<SendGridNotificationProviderSettings>(extendedData.GetValue(SendGridNotificationProviderSettingsKey));
        }
    }
}