﻿(function() {
    angular.module("merchello.plugins.sendGridNotifications", [
        'merchello.models',
        'merchello.services'
    ]);

    angular.module("merchello.plugins").requires.push("merchello.plugins.sendGridNotifications");
})();