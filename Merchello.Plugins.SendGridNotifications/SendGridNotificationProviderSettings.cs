﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Merchello.Plugins.SendGridNotifications
{
    public class SendGridNotificationProviderSettings
    {
        public string ApiKey { get; set; }

        public string TemplateId { get; set; }
    }
}