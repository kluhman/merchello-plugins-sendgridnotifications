﻿using System;
using System.Collections.Generic;
using System.Linq;

using Merchello.Core.Gateways;
using Merchello.Core.Gateways.Notification;
using Merchello.Core.Models;
using Merchello.Core.Services;

using Umbraco.Core.Cache;
using Umbraco.Core.Logging;

namespace Merchello.Plugins.SendGridNotifications
{
    [GatewayProviderActivation("52D0345A-B301-43AF-972C-70A276C40ED1", "Send Grid Notification Provider", "Send Grid Notification Provider")]
    [GatewayProviderEditor("SMTP Notification Configuration", "~/App_Plugins/Merchello.Plugins.SendGridNotifications/provider-settings/provider-settings.template.html")]
    public class SendGridNotificationGatewayProvider : NotificationGatewayProviderBase
    {
        private static readonly ICollection<IGatewayResource> _availableResources = new List<IGatewayResource>
        {
            new GatewayResource("Email", "Email Message")
        };

        public SendGridNotificationGatewayProvider(IGatewayProviderService gatewayProviderService, IGatewayProviderSettings gatewayProviderSettings, IRuntimeCacheProvider runtimeCacheProvider) : base(gatewayProviderService, gatewayProviderSettings, runtimeCacheProvider)
        {
        }

        public override IEnumerable<IGatewayResource> ListResourcesOffered()
        {
            var usedResources = NotificationMethods.Select(m => m.ServiceCode).ToList();
            return _availableResources.Where(r => !usedResources.Contains(r.ServiceCode));
        }

        public override INotificationGatewayMethod CreateNotificationMethod(IGatewayResource gatewayResource, string name, string serviceCode)
        {
            var attempt = GatewayProviderService.CreateNotificationMethodWithKey(GatewayProviderSettings.Key, name, serviceCode);
            if (attempt.Success)
            {
                return new SendGridEmailNotificationGatewayMethod(GatewayProviderService, attempt.Result, ExtendedData);
            }

            LogHelper.Error<SendGridNotificationGatewayProvider>($"An error occurred creating {nameof(SendGridEmailNotificationGatewayMethod)}", attempt.Exception);

            throw attempt.Exception;
        }

        public override IEnumerable<INotificationGatewayMethod> GetAllNotificationGatewayMethods()
        {
            return NotificationMethods.Select(m => new SendGridEmailNotificationGatewayMethod(GatewayProviderService, m, ExtendedData));
        }
    }
}