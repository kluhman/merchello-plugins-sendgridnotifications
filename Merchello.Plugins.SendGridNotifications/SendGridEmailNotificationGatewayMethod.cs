﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Merchello.Core.Gateways.Notification;
using Merchello.Core.Models;
using Merchello.Core.Services;

using SendGrid;
using SendGrid.Helpers.Mail;

using Umbraco.Core;
using Umbraco.Core.Logging;

using Attachment = System.Net.Mail.Attachment;

namespace Merchello.Plugins.SendGridNotifications
{
    public class SendGridEmailNotificationGatewayMethod : NotificationGatewayMethodBase
    {
        private readonly SendGridNotificationProviderSettings _settings;

        public SendGridEmailNotificationGatewayMethod(IGatewayProviderService gatewayProviderService, INotificationMethod notificationMethod, ExtendedDataCollection extendedData) : base(gatewayProviderService, notificationMethod)
        {
            Mandate.ParameterNotNull(extendedData, nameof(extendedData));

            _settings = extendedData.GetSendGridNotificationSettings();
        }

        public override void PerformSend(IFormattedNotificationMessage message, IEnumerable<Attachment> attachments = null)
        {
            try
            {
                attachments = attachments ?? new List<Attachment>();
                if (!message.Recipients.Any())
                {
                    return;
                }

                var mail = new SendGridMessage
                {
                    HtmlContent = message.BodyText,
                    From = new EmailAddress(message.From),
                    Subject = message.Name
                };

                if (!string.IsNullOrWhiteSpace(_settings.TemplateId))
                {
                    mail.TemplateId = _settings.TemplateId;
                }

                foreach (var recipient in message.Recipients)
                {
                    mail.AddTo(new EmailAddress(recipient));
                }

                if (!string.IsNullOrWhiteSpace(message.ReplyTo))
                {
                    mail.SetReplyTo(new EmailAddress(message.ReplyTo));
                }

                foreach (var attachment in attachments)
                {
                    if (attachment.ContentStream.CanSeek && attachment.ContentStream.Position >= 0)
                    {
                        attachment.ContentStream.Seek(0, SeekOrigin.Begin);
                    }

                    using (var reader = new StreamReader(attachment.ContentStream))
                    {
                        mail.AddAttachment(attachment.Name, reader.ReadToEnd(), attachment.ContentType.MediaType, attachment.ContentDisposition.DispositionType, attachment.ContentId);
                    }
                }

                var client = new SendGridClient(_settings.ApiKey);
                client.SendEmailAsync(mail).Wait();
            }
            catch (Exception ex)
            {
                LogHelper.Error<SendGridEmailNotificationGatewayMethod>("An error occurred sending notification", ex);
            }
        }
    }
}