﻿(function () {
    angular.module("merchello.plugins.sendGridNotifications").controller('sendGridNotificationProviderSettingsController', ['$scope', function ($scope) {
        $scope.sendGridProviderSettings = {};
        var extendedDataKey = 'sendGridNotificationProviderSettings';

        if ($scope.dialogData.provider.extendedData.items.length > 0) {
            var settingsString = $scope.dialogData.provider.extendedData.getValue(extendedDataKey);
            $scope.sendGridProviderSettings = angular.fromJson(settingsString);
        }

        $scope.$watch(function () { return $scope.sendGridProviderSettings; }, function (settings) {
            $scope.dialogData.provider.extendedData.setValue(extendedDataKey, angular.toJson(settings));
        }, true);
    }]);
})();